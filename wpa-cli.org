# -*- coding: utf-8; mode: org-development-elisp; -*-
#+title: wpa-cli
#+subtitle: Part of the =wpa= Emacs Lisp package
#+author: =#<PERSON akater A24961DE3ADD04E057ADCF4599555CE6F2E1B21D>=
#+property: header-args :tangle wpa-cli.el :lexical t
#+startup: nologdone show2levels
#+todo: TODO(t@) HOLD(h@/!) | DONE(d@)
#+todo: BUG(b@/!) | FIXED(x@)
#+todo: TEST(u) TEST-FAILED(f) | TEST-PASSED(p)
#+todo: DEPRECATED(r@) | OBSOLETE(o@)

* Dependencies
#+begin_src elisp :results none
(eval-when-compile (require 'rx))
(require 'cl-seq)
(eval-when-compile (require 'cl-macs))
(require 'comint)
(require 'minibuffer)
(require 'pcomplete)
(eval-when-compile (require 'mmxx-macros-anaphora))
(eval-when-compile (require 'akater-misc-rest)) ; with-ellipsis-done-message
(require 'akater-misc-essentials)
(require 'wpa-util)
(require 'wpa-ecli)
#+end_src

* Credits
Thank You
- to Mickey Petersen for guides on completion
- to John Wiegley for Eshell command parser

* Notes
We want the contents of wpa-cli buffer to look reasonably consistent with what a common terminal session would look like, or at least provide option to work like this.  However, some prettification is safe to do, e.g. reporting unknown commands solely in minibuffer.

* TODO Report “Unknown command” solely in minibuffer
- State "TODO"       from              [2020-08-06 Thu 09:55]
* TODO Optionally support piggybacking on native =wpa_cli= completion
- State "TODO"       from              [2020-08-28 Fri 20:46]
* Constants
#+begin_src elisp :results none
(defconst wpa-cli-prompt-regexp (rx line-start "> "))
#+end_src

* Global Variables
** arguments
#+begin_src elisp :results none
(defvar wpa-cli-arguments nil)
#+end_src

** file-path
#+begin_src elisp :results none
(defvar wpa-cli-file-path "wpa_cli")
#+end_src

** scan-results-buffer-name
#+begin_src elisp :results none
(defconst wpa-cli-scan-results-buffer-name "*wpa scan-results*")
#+end_src

** font-lock-keywords
#+begin_src elisp :results none
(defvar wpa-cli-font-lock-keywords
  `((,(rx line-start "Selected interface " "'" (group (one-or-more (not ?\'))) "'")
     (0 '(face nil compilation-message nil help-echo nil mouse-face nil) t)
     (1 compilation-info-face))
    (,(rx line-start "CTRL-REQ-" (one-or-more (not ?-)) ?- (one-or-more (not ?-)) ":")
     ;; i don't know what these mean
     (0 '(face nil compilation-message nil help-echo nil mouse-face nil) t)
     (1 font-lock-function-name-face)))
  "Additional things to highlight in wpa-cli mode.
This gets tacked on the end of the generated expressions.")
#+end_src

* parse-arguments
** Prerequisites
*** use-eshell-parser
#+begin_src elisp :results none
(defvar wpa-cli-use-eshell-parser t)
#+end_src

*** complete-parse-argument
**** Prerequisites
***** Global Variables
#+begin_src elisp :results none
(defvar wpa-cli-nested-argument nil)
(defvar wpa-cli-current-argument nil)
(defvar wpa-cli-arg-listified nil)
(defvar wpa-cli-current-modifiers nil)
#+end_src

***** resolve
****** Prerequisites
******* to-flat-string
******** Prerequisites
********* stringify
********** Prerequisites
#+begin_src elisp :results none
(defvar wpa-cli-stringify-t t
  "If non-nil, the string representation of t is \"t\".
If nil, t will be represented only in the exit code of the function,
and not printed as a string.  This causes Lisp functions to behave
similarly to external commands, as far as successful result output."
  ;; :type 'boolean
  ;; :group 'eshell-util
  )
#+end_src

********** Definition
#+begin_src elisp :results none
(defun wpa-cli-stringify (object)
  "Convert OBJECT into a string value."
  (cond
   ((stringp object) object)
   ((and (listp object)
	 (not (eq object nil)))
    (let ((string (pp-to-string object)))
      (substring string 0 (1- (length string)))))
   ((numberp object)
    (number-to-string object))
   (t
    (unless (and (eq object t)
		 (not wpa-cli-stringify-t))
      (pp-to-string object)))))
#+end_src

******** Definition
#+begin_src elisp :results none
(defun wpa-cli-to-flat-string (value)
  "Make value a string.  If separated by newlines change them to spaces."
  (let ((text (wpa-cli-stringify value)))
    (if (string-match "\n+\\'" text)
	(setq text (replace-match "" t t text)))
    (while (string-match "\n+" text)
      (setq text (replace-match " " t t text)))
    text))
#+end_src

****** Definition
#+begin_src elisp :results none
(defun wpa-cli-resolve-current-argument ()
  "If there are pending modifications to be made, make them now."
  (when wpa-cli-current-argument
    (when wpa-cli-arg-listified
      (let ((parts wpa-cli-current-argument))
	(while parts
	  (unless (stringp (car parts))
	    (setcar parts
		    (list 'wpa-cli-to-flat-string (car parts))))
	  (setq parts (cdr parts)))
	(setq wpa-cli-current-argument
	      (list 'wpa-cli-convert
		    (append (list 'concat) wpa-cli-current-argument))))
      (setq wpa-cli-arg-listified nil))
    (while wpa-cli-current-modifiers
      ;; (push (pop wpa-cli-current-modifiers)
      ;;       wpa-cli-current-argument)
      (setq wpa-cli-current-argument
            (list (car wpa-cli-current-modifiers)
                  wpa-cli-current-argument)
            
	    wpa-cli-current-modifiers
            (cdr wpa-cli-current-modifiers))))
  (setq wpa-cli-current-modifiers nil))
#+end_src

***** parse-argument-hook
****** Prerequisites
******* number-regexp
******** Prerequisites
********* convert-numeric-arguments
#+begin_src elisp :results none
(defvar wpa-cli-convert-numeric-arguments t
  "If non-nil, converting arguments of numeric form to Lisp numbers.
Numeric form is tested using the regular expression
`wpa-cli-number-regexp'.

NOTE: If you find that numeric conversions are interfering with the
specification of filenames (for example, in calling `find-file', or
some other Lisp function that deals with files, not numbers), add the
following in your init file:

  (put \\='find-file \\='eshell-no-numeric-conversions t)

Any function with the property `wpa-cli-no-numeric-conversions' set to
a non-nil value, will be passed strings, not numbers, even when an
argument matches `wpa-cli-number-regexp'."
  ;; :type 'boolean
  ;; :group 'eshell-util
  )
#+end_src

******** Definition
#+begin_src elisp :results none
(defvar wpa-cli-number-regexp "-?\\([0-9]*\\.\\)?[0-9]+\\(e[-0-9.]+\\)?"
  "Regular expression used to match numeric arguments.
If `wpa-cli-convert-numeric-arguments' is non-nil, and an argument
matches this regexp, it will be converted to a Lisp number, using the
function `string-to-number'."
  ;; :type 'regexp
  ;; :group 'eshell-util
  )
#+end_src

******* arg-delimiter
******** Prerequisites
#+begin_src elisp :results none
(defvar wpa-cli-delimiter-argument-list '(?\; ?& ?\| ?\> ?\s ?\t ?\n)
  "List of characters to recognize as argument separators."
  ;; :type '(repeat character)
  ;; :group 'eshell-arg
  )
#+end_src

******** Definition
#+begin_src elisp :results none
(defsubst wpa-cli-arg-delimiter (&optional pos)
  "Return non-nil if POS is an argument delimiter.
If POS is nil, the location of point is checked."
  (let ((pos (or pos (point))))
    (or (= pos (point-max))
	(memq (char-after pos) wpa-cli-delimiter-argument-list))))
#+end_src

******* inside-quote-regexp, outside-quote-regexp
#+begin_src elisp :results none
(defvar wpa-cli-inside-quote-regexp nil)
(defvar wpa-cli-outside-quote-regexp nil)
#+end_src

******* special-chars-inside-quoting
#+begin_src elisp :results none
(defvar wpa-cli-special-chars-inside-quoting '(?\\ ?\")
  "Characters which are still special inside double quotes."
  ;; :type '(repeat character)
  ;; :group 'eshell-arg
  )
#+end_src

******* special-chars-outside-quoting
#+begin_src elisp :results none
(defvar wpa-cli-special-chars-outside-quoting
  (append wpa-cli-delimiter-argument-list '(?# ?! ?\\ ?\" ?\'))
  "Characters that require escaping outside of double quotes.
Without escaping them, they will introduce a change in the argument."
  ;; :type '(repeat character)
  ;; :group 'eshell-arg
  )
#+end_src

******* current-quoted
#+begin_src elisp :results none
(defvar wpa-cli-current-quoted nil)
#+end_src

******* finish-arg
******** Definition
#+begin_src elisp :results none
(defun wpa-cli-finish-arg (&optional arguments)
  "Finish the current argument being processed.
If any ARGUMENTS are specified, they will be added to the final
argument list in place of the value of the current argument."
  (when arguments
    (if (= (length arguments) 1)
        (setq wpa-cli-current-argument (car arguments))
      (cl-assert (and (not wpa-cli-arg-listified)
                      (not wpa-cli-current-modifiers)))
      (setq wpa-cli-current-argument
            (cons 'wpa-cli-splice-immediately arguments))))
  (throw 'wpa-cli-arg-done t))
#+end_src

******* parse-double-quote
******** Prerequisites
********* find-delimiter
********** Definition
#+begin_src elisp :results none
(defun wpa-cli-find-delimiter
  (open close &optional bound reverse-p backslash-p)
  "From point, find the CLOSE delimiter corresponding to OPEN.
The matching is bounded by BOUND.
If REVERSE-P is non-nil, process the region backwards.
If BACKSLASH-P is non-nil, and OPEN and CLOSE are the same character,
then quoting is done by a backslash, rather than a doubled delimiter."
  (save-excursion
    (let ((depth 1)
	  (bound (or bound (point-max))))
      (if (if reverse-p
	      (eq (char-before) close)
	    (eq (char-after) open))
	  (forward-char (if reverse-p -1 1)))
      (while (and (> depth 0)
		  (funcall (if reverse-p '> '<) (point) bound))
	(let ((c (if reverse-p (char-before) (char-after))) nc)
	  (cond ((and (not reverse-p)
		      (or (not (eq open close))
			  backslash-p)
		      (eq c ?\\)
		      (setq nc (char-after (1+ (point))))
		      (or (eq nc open) (eq nc close)))
		 (forward-char 1))
		((and reverse-p
		      (or (not (eq open close))
			  backslash-p)
		      (or (eq c open) (eq c close))
		      (eq (char-before (1- (point)))
			  ?\\))
		 (forward-char -1))
		((eq open close)
		 (if (eq c open)
		     (if (and (not backslash-p)
			      (eq (if reverse-p
				      (char-before (1- (point)))
				    (char-after (1+ (point)))) open))
			 (forward-char (if reverse-p -1 1))
		       (setq depth (1- depth)))))
		((= c open)
		 (setq depth (+ depth (if reverse-p -1 1))))
		((= c close)
		 (setq depth (+ depth (if reverse-p 1 -1))))))
	(forward-char (if reverse-p -1 1)))
      (if (= depth 0)
	  (if reverse-p (point) (1- (point)))))))
#+end_src

******** Definition
#+begin_src elisp :results none
(defun wpa-cli-parse-double-quote ()
  "Parse a double quoted string, which allows for variable interpolation."
  (when (eq (char-after) ?\")
    (let* ((end (wpa-cli-find-delimiter ?\" ?\" nil nil t))
	   (wpa-cli-current-quoted t))
      (if (not end)
	  (throw 'wpa-cli-incomplete ?\")
	(prog1
	    (save-restriction
	      (forward-char)
	      (narrow-to-region (point) end)
	      (let ((arg (wpa-cli-parse-argument)))
		(if (eq arg nil)
		    ""
		  (list 'wpa-cli-escape-arg arg))))
	  (goto-char (1+ end)))))))
#+end_src

******* parse-delimiter
******** Prerequisites
********* separate-commands
********** Definition
#+begin_src elisp :results none
(defun wpa-cli-separate-commands ( terms separator
                                   &optional reversed last-terms-sym)
  "Separate TERMS using SEPARATOR.
If REVERSED is non-nil, the list of separated term groups will be
returned in reverse order.  If LAST-TERMS-SYM is a symbol, its value
will be set to a list of all the separator operators found (or (nil)
if none)."
  (let ((sub-terms (list t))
	(wpa-cli-sep-terms (list t))
	subchains)
    (while terms
      (if (and (consp (car terms))
	       (eq (caar terms) 'wpa-cli-operator)
	       (string-match (concat "^" separator "$")
			     (nth 1 (car terms))))
	  (progn
	    (nconc wpa-cli-sep-terms (list (nth 1 (car terms))))
	    (setq subchains (cons (cdr sub-terms) subchains)
		  sub-terms (list t)))
	(nconc sub-terms (list (car terms))))
      (setq terms (cdr terms)))
    (if (> (length sub-terms) 1)
	(setq subchains (cons (cdr sub-terms) subchains)))
    (if reversed
	(progn
	  (if last-terms-sym
	      (set last-terms-sym (reverse (cdr wpa-cli-sep-terms))))
	  subchains)                    ; already reversed
      (if last-terms-sym
	  (set last-terms-sym (cdr wpa-cli-sep-terms)))
      (nreverse subchains))))
#+end_src

******** Definition
#+begin_src elisp :results none
(defun wpa-cli-parse-delimiter ()
  "Parse an argument delimiter, which is essentially a command operator."
  ;; this `wpa-cli-operator' keyword gets parsed out by
  ;; `wpa-cli-separate-commands'.  Right now the only possibility for
  ;; error is an incorrect output redirection specifier.
  (when (looking-at "[&|;\n]\\s-*")
    (let ((end (match-end 0)))
    (if wpa-cli-current-argument
	(wpa-cli-finish-arg)
      (wpa-cli-finish-arg
       (prog1
	   (list 'wpa-cli-operator
		 (cond
		  ((eq (char-after end) ?\&)
		   (setq end (1+ end)) "&&")
		  ((eq (char-after end) ?\|)
		   (setq end (1+ end)) "||")
		  ((eq (char-after) ?\n) ";")
		  (t
		   (char-to-string (char-after)))))
	 (goto-char end)))))))
#+end_src

****** Definition
#+begin_src elisp :results none
(defvar wpa-cli-parse-argument-hook
  (list
   ;; a term such as #<buffer NAME>, or #<process NAME> is a buffer
   ;; or process reference
   ;; 'eshell-parse-special-reference

   ;; numbers convert to numbers if they stand alone
   (function
    (lambda ()
      (when (and (not wpa-cli-current-argument)
		 (not wpa-cli-current-quoted)
		 (looking-at wpa-cli-number-regexp)
		 (wpa-cli-arg-delimiter (match-end 0)))
	(goto-char (match-end 0))
	(let ((str (match-string 0)))
	  (if (> (length str) 0)
	      (add-text-properties 0 (length str) '(number t) str))
	  str))))

   ;; parse any non-special characters, based on the current context
   (function
    (lambda ()
      (unless wpa-cli-inside-quote-regexp
	(setq wpa-cli-inside-quote-regexp
	      (format "[^%s]+"
		      (apply 'string wpa-cli-special-chars-inside-quoting))))
      (unless wpa-cli-outside-quote-regexp
	(setq wpa-cli-outside-quote-regexp
	      (format "[^%s]+"
		      (apply 'string wpa-cli-special-chars-outside-quoting))))
      (when (looking-at (if wpa-cli-current-quoted
			    wpa-cli-inside-quote-regexp
			  wpa-cli-outside-quote-regexp))
	(goto-char (match-end 0))
	(let ((str (match-string 0)))
	  (if str
	      (set-text-properties 0 (length str) nil str))
	  str))))

   ;; whitespace or a comment is an argument delimiter
   (function
    (lambda ()
      (let (comment-p)
	(when (or (looking-at "[ \t]+")
		  (and (not wpa-cli-current-argument)
		       (looking-at "#\\([^<'].*\\|$\\)")
		       (setq comment-p t)))
	  (if comment-p
	      (add-text-properties (match-beginning 0) (match-end 0)
				   '(comment t)))
	  (goto-char (match-end 0))
	  (wpa-cli-finish-arg)))))

   ;; parse backslash and the character after
   ;; 'eshell-parse-backslash

   ;; text beginning with ' is a literally quoted
   ;; 'eshell-parse-literal-quote

   ;; text beginning with " is interpolably quoted
   'wpa-cli-parse-double-quote

   ;; argument delimiter
   'wpa-cli-parse-delimiter)
  "Define how to process Eshell command line arguments.
When each function on this hook is called, point will be at the
current position within the argument list.  The function should either
return nil, meaning that it did no argument parsing, or it should
return the result of the parse as a sexp.  It is also responsible for
moving the point forward to reflect the amount of input text that was
parsed.

If no function handles the current character at point, it will be
treated as a literal character."
  ;; :type 'hook
  ;; :group 'eshell-arg
  )
#+end_src

**** Definition
#+begin_src elisp :results none
(defun wpa-cli-parse-argument ()
  "Get the next argument.  Leave point after it."
  (let* ((outer (null wpa-cli-nested-argument))
	 (arg-begin (and outer (point)))
	 (wpa-cli-nested-argument t)
	 wpa-cli-current-argument
	 wpa-cli-current-modifiers
	 wpa-cli-listified)
    (catch 'wpa-cli-arg-done
      (while (not (eobp))
	(let ((result
	       (or (run-hook-with-args-until-success
		    'wpa-cli-parse-argument-hook)
		   (prog1 (char-to-string (char-after))
		     (forward-char)))))
	  (if (not wpa-cli-current-argument)
	      (setq wpa-cli-current-argument result)
	    (unless wpa-cli-listified
	      (setq wpa-cli-current-argument
		    (list wpa-cli-current-argument)
		    wpa-cli-listified t))
	    (nconc wpa-cli-current-argument (list result))))))
    (when (and outer wpa-cli-current-argument)
      (add-text-properties arg-begin (1+ arg-begin)
			   '(arg-begin t rear-nonsticky
				       (arg-begin arg-end)))
      (add-text-properties (1- (point)) (point)
			   '(arg-end t rear-nonsticky
				     (arg-end arg-begin))))
    (wpa-cli-resolve-current-argument)
    wpa-cli-current-argument))
#+end_src

** Definition
#+begin_src elisp :results none
(defun wpa-cli-parse-arguments (beg end)
  "Parse all of the arguments at point from BEG to END.
Returns the list of arguments in their raw form.
Point is left at the end of the arguments."
  (if wpa-cli-use-eshell-parser
      (save-excursion
        (save-restriction
          (goto-char beg)
          (narrow-to-region beg end)
          (let ((args (list t))
	        delim)
            (with-silent-modifications
              (remove-text-properties (point-min) (point-max)
                                      '(arg-begin nil arg-end nil))
              (if (setq
                   delim
                   (catch 'wpa-cli-incomplete
                     (while (not (eobp))
                       (let* ((here (point))
                              (arg (wpa-cli-parse-argument)))
                         (if (= (point) here)
                             (error "Failed to parse argument `%s'"
                                    (buffer-substring here (point-max))))
                         (when arg
                       (nconc args
                              (if (eq (car-safe arg)
                                      'wpa-cli-splice-immediately)
                                  (cdr arg)
                                (list arg))))))))
                  (throw 'wpa-cli-incomplete (if (listp delim)
                                                 delim
                                               (list delim (point) (cdr args)))))
              (cdr args)))))
    (list nil)))
#+end_src

* complete-parse-arguments
** Definition
#+begin_src elisp :results none
(declare-function eshell-do-eval "esh-cmd")
(defun wpa-cli-complete-parse-arguments ()
  "Parse the command line arguments for `pcomplete-argument'."
  (let ((end (point-marker))
	(begin (save-excursion (goto-char (line-beginning-position)) (point)))
	(posns (list t))
	args delim)
    (when (memq this-command '(pcomplete-expand
			       pcomplete-expand-and-complete))
      (if (= begin end)
	  (end-of-line))
      (setq end (point-marker)))
    (if (setq delim
	      (catch 'wpa-cli-incomplete
		(ignore
		 (setq args (wpa-cli-parse-arguments begin end)))))
	(cond ((memq (car delim) '(?\{ ?\<))
	       (setq begin (1+ (cadr delim))
		     args (wpa-cli-parse-arguments begin end)))
	      ;; ((eq (car delim) ?\()
	      ;;  (eshell-complete-lisp-symbol)
	      ;;  (throw 'pcompleted t))
	      (t
	       (insert-and-inherit "\t")
	       (throw 'pcompleted t))))
    (when (get-text-property (1- end) 'comment)
      (insert-and-inherit "\t")
      (throw 'pcompleted t))
    (let ((pos begin))
      (while (< pos end)
	(if (get-text-property pos 'arg-begin)
	    (nconc posns (list pos)))
	(setq pos (1+ pos))))
    (setq posns (cdr posns))
    (cl-assert (= (length args) (length posns)))
    (let ((a args)
	  (i 0)
	  l)
      (while a
	(if (and (consp (car a))
		 (eq (caar a) 'wpa-cli-operator))
	    (setq l i))
	(setq a (cdr a) i (1+ i)))
      (and l
	   (setq args (nthcdr (1+ l) args)
		 posns (nthcdr (1+ l) posns))))
    (cl-assert (= (length args) (length posns)))
    (when (and args (eq (char-syntax (char-before end)) ?\s)
	       (not (eq (char-before (1- end)) ?\\)))
      (nconc args (list ""))
      (nconc posns (list (point))))
    (cons (mapcar
	   (function
	    (lambda (arg)
	      (let ((val
		     (if (listp arg)
			 (let ((result
				(eshell-do-eval
				 (list 'wpa-cli-commands arg) t)))
			   (cl-assert (eq (car result) 'quote))
			   (cadr result))
		       arg)))
		(if (numberp val)
		    (setq val (number-to-string val)))
		(or val ""))))
	   args)
	  posns)))
#+end_src

* commands-collection
** Prerequisites
*** get-local-commands-collection
**** Definition
#+begin_src elisp :results none
(defun wpa-cli-get-local-commands-collection ()
  (mapcar #'car (wpa-ecli-help)))
#+end_src

** Definition
#+begin_src elisp :results none
(defvar-local wpa-cli-commands-collection
  nil
  ;; (with-temp-buffer
  ;;     (shell-command (sh-wrap* `(,wpa-cli-file-path :help))
  ;;                    (current-buffer))
  ;;     (mapcar #'car (wpa-cli-parse-help-page (current-buffer))))
  )
#+end_src

* get-default-buffer
** Definition
#+begin_src elisp :results none
(defalias 'wpa-cli-get-default-buffer 'wpa-ecli-get-default-buffer)
#+end_src

* get-version
** Definition
#+begin_src elisp :results none
(defun wpa-cli-get-version ()
  "Return running wpa_cli version string in Emacs-complaint format."
  (with-temp-buffer
    (let ((buffer (wpa-cli-get-default-buffer)) (start 0) (end 0))
      (with-current-buffer buffer
        (save-excursion
          (goto-char (point-min))
          (re-search-forward "wpa_cli v")
          (setq start (point) end (line-end-position))))
      (insert-buffer-substring-no-properties buffer start end))
    (goto-char (point-min))
    (buffer-substring-no-properties
     (point)
     (progn (re-search-forward (rx (one-or-more (any digit ?\.))))
            (cond
             ((looking-at "-devel") (insert "snapshot"))
             ((looking-at (rx line-end)))
             (t (insert "unknown")))
            (point)))))
#+end_src

** Tests
*** Should work with ~version<~
This test is disabled because it's not available in sandbox.
#+begin_src elisp :tangle no :results code :wrap example elisp
(version< "0.99" (wpa-cli-get-version))
#+end_src

#+EXPECTED:
#+begin_example elisp
t
#+end_example

* Mode Map
** Definition
#+begin_src elisp :results none
(defvar wpa-cli-mode-map
  (aprog1 (nconc (make-sparse-keymap) comint-mode-map)
    (define-key it "\t" 'completion-at-point))
  "Basic mode map for `wpa-cli'")
#+end_src

* Major Mode
** Prerequisites
*** send-input-on-some-messages
**** TODO Smarter send input on =CTRL-EVENT= messages
- State "TODO"       from              [2020-08-27 Thu 13:43] \\
  Sending input on any =CTRL-EVENT= message would add too many empty strings between some messages that better be left contiguous.  And I'd rather only send input for =BEACON-LOSS= when the buffer is displayed for the same reason of somewhat wasted screen space.
**** Definition
#+begin_src elisp :results none
(defun wpa-cli-send-input-on-some-messages (string)
  (when (or (string-match (rx "CTRL-EVENT-"
                              (or "SCAN-RESULTS" "CONNECTED"
                                  "NETWORK-NOT-FOUND"
                                  "BEACON-LOSS" "TERMINATING"))
                          string)
            ;; on the following match,
            ;; prompt is not highlighted after input is sent
            (string-match (rx "Connection to wpa_supplicant re-established")
                          string))
    (comint-send-input)))
#+end_src

*** cli-version
#+begin_src elisp :results none
(defvar-local wpa-cli-version "")
#+end_src

** Definition
#+begin_src elisp :results none
(define-derived-mode wpa-cli-mode comint-mode "wpa"
  "Major mode for `wpa-cli'.

\\<wpa-mode-map>"
  nil "wpa"
  (setq comint-prompt-regexp wpa-cli-prompt-regexp)
  (setq comint-prompt-read-only t)
  (setq-local comint-output-filter-functions
              (list
               ;; 'ansi-color-process-output
                    'comint-postoutput-scroll-to-bottom
                    'wpa-cli-send-input-on-some-messages
                    ;; 'comint-watch-for-password-prompt
                    ))
  (sleep-for 0.500)
  (set (make-local-variable 'wpa-cli-version)
       (wpa-cli-get-version))
  (with-akater-ellipsis-done-message "Caching commands"
    (setq-local wpa-cli-commands-collection
                (wpa-cli-get-local-commands-collection)))
  (set (make-local-variable 'pcomplete-command-completion-function)
       (lambda () (pcomplete-here wpa-cli-commands-collection)))
  (set (make-local-variable 'pcomplete-parse-arguments-function)
       'wpa-cli-complete-parse-arguments)
  (add-hook 'completion-at-point-functions
            #'pcomplete-completions-at-point nil t)
  (set (make-local-variable 'paragraph-separate) "\\'")
  (set (make-local-variable 'font-lock-defaults)
       '(wpa-cli-font-lock-keywords t))
  (set (make-local-variable 'paragraph-start) wpa-cli-prompt-regexp)
  (get-buffer-create wpa-ecli-status-buffer-name)
  (with-current-buffer (get-buffer-create
                        wpa-cli-scan-results-buffer-name)
    (wpa-cli-scan-results-mode)))
#+end_src

* (the following uses basic procedures from wpa-ecli)
* pcomplete
** interface
*** TODO If there is only one interface available, insert it immediately and report uniqueness in minibuffer
- State "TODO"       from              [2020-08-06 Thu 21:35]
*** Definition
#+begin_src elisp :results none
(defun pcomplete/wpa-cli-mode/interface ()
  "Completion for `interface' in `wpa-cli'"
  ;; Completion for the command argument.
  (pcomplete-here* (wpa-ecli-interface)))
#+end_src

** help
*** Definition
#+begin_src elisp :results none
(defun pcomplete/wpa-cli-mode/help ()
  "Completion for `help' in `wpa-cli'"
  ;; Completion for the command argument.
  (pcomplete-here* wpa-cli-commands-collection))
#+end_src

* scan-results-mode
** Prerequisites
#+begin_src elisp :results none
(defcustom wpa-cli-scan-results-ssid-reserved-space 20
  "How many chars to reserve in table *wpa scan-results*"
  :type 'integer
  :group 'wpa)
#+end_src

** Definition
#+begin_src elisp :results none
(define-derived-mode wpa-cli-scan-results-mode tabulated-list-mode "wpa:scan"
  "Major mode for the list of wireless access points."
  ;; :group 'wpa
  (setq-local line-move-visual nil)
  (setq tabulated-list-format `[("ssid" ,wpa-cli-scan-results-ssid-reserved-space nil)
                                ("bssid" 17 t)
                                ("freq" 4 t :right-align t)
                                ("lvl" 3 t :right-align t)
                                ("flags" 0 nil)]
        tabulated-list-padding 1)
  (tabulated-list-init-header))
#+end_src

* TODO Establish wpa-cli buffers initiation and naming
- State "TODO"       from              [2020-08-28 Fri 15:51] \\
  At the moment, ~wpa-cli-run~ refers to the buffer name hardcoded in =wpa-ecli.org=.
* run
** Definition
#+begin_src elisp :results none
;;;###autoload
(defun wpa-cli-run ()
  (interactive)
  (let ((wpa-program wpa-cli-file-path)
        (buffer (comint-check-proc "wpa")))
    ;; pop to the "*wpa*" buffer if the process is dead, the
    ;; buffer is missing or it's got the wrong mode.
    (pop-to-buffer-same-window
     (if (or buffer (not (derived-mode-p 'wpa-mode))
             (comint-check-proc (current-buffer)))
         (get-buffer-create (or buffer wpa-ecli-default-buffer-name))
       (current-buffer)))
    ;; create the comint process if there is no buffer.
    (unless buffer
      (apply 'make-comint-in-buffer "wpa" buffer
             wpa-program wpa-cli-arguments)
      (wpa-cli-mode))))
#+end_src

* running-p
** Definition
#+begin_src elisp :results none
(defun wpa-cli-running-p ()
  (get-buffer wpa-ecli-default-buffer-name))
#+end_src

* (uses scan procedures from wpa-ecli)
** scan-results-update-table
*** Definition
#+begin_src elisp :results none
(defun wpa-cli-scan-results-update-table ()
  (interactive)
  (aprog1 (wpa-ecli-scan-results 'tabulated-list-mode)
    (with-current-buffer (get-buffer wpa-cli-scan-results-buffer-name)
      (setq-local tabulated-list-entries it)
      (tabulated-list-print))))
#+end_src

** scan
*** Definition
#+begin_src elisp :results none
;;;###autoload
(defun wpa-cli-scan ()
  (interactive)
  (wpa-ecli-scan)
  (wpa-cli-scan-results-update-table)
  (display-buffer wpa-cli-scan-results-buffer-name))
#+end_src

* state
** connected-p-string
*** Definition
#+begin_src elisp :results none
(defun wpa-connected-p-string ( human-readable-network-name-or-nil
                                &optional prefix)
  (concat (aif prefix (concat (akater-misc-ensure-string it) ": ")
            "")
          (aif human-readable-network-name-or-nil
              (format "connected to %s" it)
            "not connected")))
#+end_src

** connected-p
*** Definition
#+begin_src elisp :results none
(defun wpa-connected-p ()
  (interactive)
  (aprog1 (wpa-util-human-readable-network-name
           (wpa-ecli-current-bssid)
           (wpa-ecli-current-ssid))
    (message (wpa-connected-p-string it 'wpa))))
#+end_src

* misc
** TODO parse-table
- State "TODO"       from              [2020-08-25 Tue 23:30] \\
  Reuse loop from ~wpa-ecli-scan-results~

Example:
#+begin_src elisp :tangle no :results code :wrap example elisp
(wpa-parse-table
 '(network-id ssid bssid flags)
 "0 Mincer any [CURRENT]
1 Lizard any [DISABLED]")
#+end_src
** scan-result-get
*** Notes
Gets properties of data provided by =wpa_cli='s =scan_results=, in particular ~wpa-cli-completing-read-access-point~.

*** bssid
#+begin_src elisp :results none
(defun wpa-cli-scan-result-get-bssid (scan-result) (car scan-result))
#+end_src

*** ssid
#+begin_src elisp :results none
(defun wpa-cli-scan-result-get-ssid (scan-result) (elt scan-result 4))
#+end_src

** network-disable
*** Definition
#+begin_src elisp :results none
(defun wpa-cli-network-disable (network)
  (when (wpa-ecli-disable-network
         (wpa-ecli-network-get-id network))
    network))
#+end_src

* (uses misc network operations from wpa-ecli)
** disable-current-network
*** Definition
#+begin_src elisp :results none
(defun wpa-disable-current-network ()
  (wpa-cli-network-disable
   (cl-find-if #'wpa-ecli-network-current-p
               (wpa-ecli-list-networks))))
#+end_src

** notes on BSSID

BSSID means “Basic Service Set Identifier”.  It is usually the MAC address but not neccesarily so.

[[https://stackoverflow.com/questions/31524769/can-bssid-be-used-as-an-unique-identifier][From]] StackOverflow:
#+begin_quote
BSSID in a beacon frame should be good enough to identify unique access
points as it is actually the MAC address. Normally it is unique for every
NIC. However, somebody could implement there own access point with a device
that allows manual change of the NIC's MAC address, this is not the case in
typical access point devices but something knowledgeable people can do.

However, you must know that two AP's with different BSSID's (different Basic
Service Sets) can belong to the same WLAN, with the same SSID. This is when
2 or more BSS's form an Extended Service Set (ESS)
#+end_quote

** completing-read-access-point
*** Definition
#+begin_src elisp :results none
(defun wpa-cli-completing-read-access-point (prompt &optional do-not-scan)
  (let* ((choices
          (amapcar (cons (wpa-util-human-readable-network-name
                          (wpa-cli-scan-result-get-bssid it)
                          (wpa-cli-scan-result-get-ssid it))
                         it)
                   (progn
                     (unless do-not-scan (wpa-ecli-scan))
                     (wpa-ecli-scan-results
                      'wpa-cli)))))
    (cdr (assoc (completing-read prompt choices) choices))))
#+end_src

** scan-and-connect
*** Notes
“Connect to a scan result” is semantically different compared to “connect to a network registered in currently running wpa_supplicant” or “connect to access point recorded in config”.  We should likely have a high-level connect fucntion that connects to a scan-result by default but allows connecting by different logic in general.  For now, only connecting to a scan result is implemented, as it was the most useful to the author.

*** Definition
**** scan-and-connect
#+begin_src elisp :results none
(defun wpa-cli-scan-and-connect (scan-result)
  (interactive
   (list
    (wpa-cli-completing-read-access-point "Wireless access point: ")))
  (wpa-ecli-ensure-and-connect-to-access-point
   (wpa-cli-scan-result-get-bssid scan-result)
   (wpa-cli-scan-result-get-ssid scan-result)))
#+end_src

* future connect
** Examples
#+begin_src elisp :tangle no :results code :wrap example elisp
(wpa-cli-connect :bssid "de:ad:00:00:be:ef"
                        :ssid "Mincer")
#+end_src

#+begin_src elisp :tangle no :results code :wrap example elisp
(wpa-cli-connect :scan-result '("94:4a:0c:ac:e4:f7" 2412 -61
  ("WPA2-PSK-CCMP" "ESS")
  "Mincer"))
#+end_src

** Definition
#+begin_src elisp :tangle no :results none
(cl-defun wpa-cli-connect/future (&key bssid ssid scan-result)
  (cond
   (scan-result
    (wpa-cli-connect :bssid (wpa-cli-scan-result-get-bssid
                                    scan-result)
                            :ssid (wpa-cli-scan-result-get-ssid
                                   scan-result)))
   ((and bssid ssid)
    ;; connect to bssid, warn if its ssid is different
    ..)
   (bssid
    ;; search for the first bssid in (cfg? list-networks?)
    ..)
   (ssid
    (warn "This is unsafe")
    ;; search for the first ssid in (cfg? list-networks?)
    ..)
   (t (message "No access point specified to connect to") nil)))
#+end_src
