;;;  -*- lexical-binding: t -*-

(defconst-with-prefix fakemake
  feature 'wpa
  authors "Dima Akater"
  first-publication-year-as-string "2019"
  org-files-in-order '("wpa-util"
                       "wpa-ecli"
                       "wpa-cli"
                       "wpa-supplicant"
                       "wpa")
  site-lisp-config-prefix "50"
  license "GPL-3")

(defvar wpa--use-flags use-flags)
